# CREATURE COLLISION - CHEAT SHEET


## Creating a variable
```Javascript
let myVariable = "some value";
let myScore = 33;
```

## Calling a function
```Javascript
someFunction();
// Calling a function with an argument
someFunction(35)
```

## Creating a javascript object (JSON)
```Javascript
let myObject = {
    someProperty: "someValue",
    someProperty2: 34
};
```

## Modifying a javascript object.
```Javascript
myObject.x = 20; // Assign the value 20 to x;
myObject.x += 20 // Increase the x value by 20;
```

## Create an ellipse
```Javascript
ellipse(x_position, y_position, x_diameter, y_diameter);
```