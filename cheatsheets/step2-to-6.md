# CREATURE COLLISION - CHEAT SHEET


## Creating a variable
```Javascript
let myVariable = "some value";
let myScore = 33;

// Create a constant
const MY_CONSTANT = "some value";
```

## Calling a function
```Javascript
someFunction();
// Calling a function with an argument
someFunction(35)
```

## Creating a javascript object (JSON)
```Javascript
let myObject = {
    someProperty: "someValue",
    someProperty2: 34
};
```

## Modifying a javascript object.
```Javascript
myObject.x = 20; // Assign the value 20 to x;
myObject.x += 20 // Increase the x value by 20;
```

## Create an ellipse
```Javascript
ellipse(x_position, y_position, x_diameter, y_diameter);
```

## Create a IF structure
```Javascript
// An if statement is used when the program needs to change behaviour when some condition is true.
if (some_condition) {
    // Gets executed when some_condition is satisfied
    my_statement1();
    my_var += 1;
}
// E.g.
if (creature.x < CANVAS_WIDTH) {
    do_something();
}
```

## Create a IF ELSE IF structure
```Javascript
// An if else statement is used when the program needs to change behaviour when some condition is true. 
// With this structure you can add as many condition as you want.

if (some_condition) {
    // Gets executed when some_condition is satisfied
    my_statement1();
    my_var += 1;
} else if (some_other_condition) {
    // These statements gets executed win the some_other_condition is true;
} else {
    // These statements gets executed when none of above mentioned conditions are true.
}
```
