let creatures = []
let kills = 0
let walls = []
let steps = 0
const CANVAS_WIDTH = 1200;
const CANVAS_HEIGHT = 800;
const NUMBER_OF_CREATURE = 500;
const NUMBER_OF_WALLS = 2;

function setup() {
  createCanvas(CANVAS_WIDTH, CANVAS_HEIGHT);
  ellipseMode(CENTER);
  createCreatures(NUMBER_OF_CREATURE)
  createWalls(NUMBER_OF_WALLS)
}

function draw() {
 
    
  background(color(240, 240, 240));
  renderWalls();    
  renderCreatures();
  renderScores();
  steps++;
  /*
  if(steps % 1500 == 0)
    addCreatures(randomNumber(50) + 10)
  */
}

function renderScores(){
  fill(color("black"));
  text("steps: " + steps + " - kills: " + kills , 10, 20);  
  let space = 20;
  let top = 50
  textSize(space);
  
  creatures.sort(function(a, b) {
    return b.strength - a.strength;
  });
  
  let position = 1;
  creatures.forEach(creature => {
    if(creature.strength){
      renderScoreLine(creature, top, position)
      top += space;
      position++;
    }
  })
  
}

function renderScoreLine(creature, top, position){
  fill(color(creature.color));
      text("[" + position + "] " + creature.strength + ": " + creature.luck + " => " + ((creature.kills/kills) * 100).toPrecision(2), 10, top); 
}

function renderWalls(){
  walls.forEach(wall => {
    fill(color(wall.color));
    rect(wall.x, wall.y, wall.width, wall.height)
  })
}

function changeDirection(creature){
  let changeX = randomNumber(2)
  let direction = randomNumber(2)

  if(changeX){      
    creature.speedY = 0
    if(direction)
        creature.speedX = 1
    else
      creature.speedX =-1      
  }
  else{      
    creature.speedX = 0
    if(direction)
        creature.speedY = 1
    else
      creature.speedY =-1      
  }
}

function renderCreatures(){
  creatures.forEach(creature => {    
    
    if(creature.strength) {    
      checkBorderCollision(creature);
      checkWallCollision(creature);
      checkCreaturesCollision(creature);        
      applyHealthCondition(creature);
      moveCreature(creature); 
      drawCreature(creature); 
      
    }
  })
}

function applyHealthCondition(creature){
  if(steps % 200 == 0){
    
    creature.diameter -= (randomNumber(5)  + Math.floor(creature.diameter/10)) 
    creature.strength -= (randomNumber(5)  + Math.floor(creature.strength/10))    
    
    if(creature.strength < 0)
      creature.strength = 0
    if(creature.diameter <= 0){
      creature.strength = 0
      kills++;
    }
    
    if(steps % 500 == 0){
      creature.diameter += randomNumber(creature.luck % 3)
      creature.strength += randomNumber(creature.luck % 3)
    }
    
  }
}

function checkWallCollision(creature){
  let radius = creature.diameter/2;
  let x = creature.x + radius
  let y = creature.y + radius
  walls.forEach(wall => {
    
    if(x >= wall.x && 
       x < wall.x + wall.width && 
       y >= wall.y && 
       y < wall.y + wall.height){
      creature.speedX *= -1
      creature.speedY *= -1
    }    
  });
    
  
}

function drawCreature(creature){
  let c = color(creature.color);
  fill(c);
  noStroke();
  ellipse(creature.x, creature.y, creature.diameter, creature.diameter);  
}

function moveCreature(creature){
   creature.steps++;
   if(creature.steps == creature.actionPoint){
     changeDirection(creature)
     creature.steps = 0
     creature.actionPoint = randomNumber(creature.actionFrequence)
   }
   creature.x += creature.speedX * creature.speed;
   creature.y += creature.speedY * creature.speed;            
}

function checkBorderCollision(creature){
  let radius = creature.diameter / 2;
  
  if (creature.x < radius){      
    creature.x = radius;
    changeDirection(creature);      
  }

  if(creature.x > CANVAS_WIDTH - radius){       
    creature.x = CANVAS_WIDTH - radius;
    changeDirection(creature);
  }

  if (creature.y < 0 + radius){      
    creature.y = radius
    changeDirection(creature);
  }

  if(creature.y > CANVAS_HEIGHT - radius){      
    creature.y = CANVAS_HEIGHT - radius;
    changeDirection(creature);
  }
}

function checkCreaturesCollision(source){
  creatures.forEach(target => { 
    if(source.id != target.id && source.strength > 0 && target.strength > 0){
      checkCreatureCollision(source, target)
    }
  })
}

function checkCreatureCollision(source, target){
  
    
  if (hasCreatureCollision(source, target)) {    
    
    sourceStrength = defineStrength(source, target)
    targetStrength = defineStrength(target, source)
    
    if(sourceStrength > targetStrength){
        eatCreature(source, target);      
    }else if(sourceStrength < targetStrength){
        eatCreature(target, source);      
    }else if(sourceStrength == targetStrength){      
      target.strength = 0;
      source.strength = 0;
      kills+=2
    }
  }
}

function hasCreatureCollision(source, target){
  let d = dist(source.x, source.y, target.x, target.y);
  return d < (source.diameter/2 + target.diameter/2)
}

function defineStrength(source, target){
  return source.strength - randomNumber(target.luck)
}

function eatCreature(hunter, prey){
  hunter.kills +=1
  hunter.strength += Math.floor(prey.strength / 10) + 1;
  hunter.diameter += Math.floor(prey.diameter / 10) + (hunter.kills %10)
  hunter.speed += 1
  prey.strength = 0;
  
  
  if(hunter.diameter > 75)
    hunter.diameter = 75
  
  if(hunter.speed > 10)
    hunter.speed = 10
  kills++;
}

function randomNumber(max){
  return Math.floor(Math.random() * max);
}

function createCreatures(number){
  creatures = []
  addCreatures(number)
}

function addCreatures(number){
  for(let i=0; i<number; i++){
    let creature = createCreature(randomNumber(100000));
    creatures.push(creature);
  }
}

function createCreature(id){
  
  let strength = randomNumber(Math.floor(steps/500)) + randomNumber(20) + 10
  
  return {
    id:id,
    x: randomNumber(CANVAS_WIDTH),
    y: randomNumber(CANVAS_HEIGHT),
    speedX: 1,
    speedY:0,
    diameter: strength,
    speed: randomNumber(5) + 1,
    steps: 0,
    actionPoint: randomNumber(100),
    actionFrequence: randomNumber(200),
    color: color(randomNumber(255), randomNumber(255), randomNumber(255)),
    strength: strength,
    kills: 0,  
    luck: randomNumber(50)
  }
}

function createWalls(number){
  walls = []
  for(let i=0; i<number; i++){
    let wall = createWall();
    walls.push(wall);
  }
}

function createWall(){
  let width = randomNumber(CANVAS_WIDTH/2) + 100;
  let height = randomNumber(20) + 10;
  
  let tmp = width;
  if(randomNumber(2)){
    width = height;
    height = tmp;
  }
  return {
    x: randomNumber(CANVAS_WIDTH),
    y: randomNumber(CANVAS_WIDTH),
    width: width,
    height: height,
    color: color(randomNumber(200)),
  }
}
