/*
---------------------------------------------------------------------------------------------
# CREATE AND MOVE A CREATURE TO THE RIGHT

- Draw a canvas and give it a background color.

- Draw a creature (ellipse)
    - Create a global creature variable
    - Use a function createCreature to add properties to the creature.
      - Use json to represent the characteristics of a creature.
    - Call the createCreature function in the drawCreature function to init and draw a creature.

- Move the creature along the x-axis.
    - Introduce moveCreature function.

TIP:
createCreature != drawCreature. 
Creating makes a new creature object while drawCreature() draws the creature on the canvas.


PLEASE COMPLETE THE INSTRUCTIONS MARKED BY ...some instruction...


---------------------------------------------------------------------------------------------
*/
//...create a creature variable called creature...

// This function is called once when the application is launched by the p5.js platform.
function setup() {
    createCanvas(800, 800);
    //...use the function createCreature() to make a new creature...
}

// The draw function repeats itself over and over by the p5.js platform.
function draw() {
    background(240);
    //...draw the creature...
    //...move the creature along the x-axis...
}

function createCreature() {
  //...create a creature object with characteristics...
  // x: 100
  // y: 100
  // diameter: 40
  creature = {
      // some_property: some_value,
      // some_property: some_value
    };
}

function drawCreature(creature) {
    let c = color("gold")
    //...create an ellipse with the size of a creature...
    fill(c); // Fill the ellipse with the color gold.
    noStroke(); // No border around the ellipse.
}

function moveCreature(creature) {
    //...move the creature one unit to the right (x-axis)...
}