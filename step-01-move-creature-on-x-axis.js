/*
# CREATE AND MOVE A CREATURE TO THE RIGHT


- INTRO: Show differents between setup and draw with console.log

- Draw a canvas and give it a background color.

- Draw a creature (ellipse)
    - Create a global creature variable
    - Use a function createCreature to add properties to the creature.
      - Use json to represent the characteristics of a creature.
    - Call the createCreature function in the drawCreature function to init and draw a creature.

- Move the creature along the x-axis.
    - Introduce moveCreature function.
*/

let creature;

// This function is called once when the application is launched by the p5.js platform.
function setup() {
    createCanvas(800, 800);
    createCreature();
}

// The draw function repeats itself over and over by the p5.js platform.
function draw() {
    background(240);
    drawCreature(creature);
    moveCreature(creature);
}

function createCreature() {
    // Create a creature object with some characteristics.
    creature = {
        x: 100,
        y: 100,
        diameter: 40
    };
}

function drawCreature(creature) {
    let c = color("gold")
    ellipse(creature.x, creature.y, creature.diameter, creature.diameter);
    fill(c); // Fill the ellipse with the color gold.
    noStroke(); // No border around the ellipse.
}

function moveCreature(creature) {
    creature.x += 1 // Increase the position on the x-axis.
}