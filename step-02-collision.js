/*
ADD WALL COLLISIONS

- Add a speedX & speedY property to the creature json object.

- Add Collision check for the x-axis.
    - Write an OK to console for each succesful boundary case.

- Add Collision check for the y-axis.
    - Use constants to give a name to difficult expressions.

- Notice all the hard codes numbers!
    - Refactor to constants.

- Remove the console statements and replace them with a change direction function!
    - Let the creature choose a random direction!
        - Change direction means making making the speed negative or positive.
        - With each collision both speedX and speedY get a random 1 or -1 value.
        - Create a function which creates a random positive integer!

- When the direction is changed, move the creature.
    - Use the new speed values to change the X and Y positions.
*/

const CANVAS_WIDTH = 400;
const CANVAS_HEIGHT = 400;
const BACKGROUND_WHITE = 240;
const MAXIMUM_SPEED = 20;

let creature;

function setup() {
    createCanvas(CANVAS_WIDTH, CANVAS_HEIGHT);
    createCreature();
}

function draw() {
    background(BACKGROUND_WHITE);
    // Before the creature is redrawn make sure there are no collisions.
    checkBorderCollision(creature);
    drawCreature(creature);
    moveCreature(creature);
}

function createCreature() {
    creature = {
        x: 100,
        y: 100,
        speedX: randomSpeed(), // Assign a random speedX value;
        speedY: randomSpeed(), // Assing a random speedY value;
        diameter: 40
    };
}

function drawCreature(creature) {
    let c = color("gold")
    ellipse(creature.x, creature.y, creature.diameter, creature.diameter);
    fill(c);
    noStroke();
}

function moveCreature(creature) {
    creature.x += creature.speedX;
    // Let the creature also use the x-axis.
    creature.y += creature.speedY;
}

function checkBorderCollision(creature) {
    let radius = creature.diameter / 2;
    const BORDER_RIGHT = CANVAS_WIDTH - radius;
    const BORDER_TOP = CANVAS_HEIGHT - radius;

    // If the center of the creature is smaller than it's radius, then it touches the left wall.
    if (creature.x < radius) {
        creature.x = radius;
        changeDirection(creature);
    }

    if (creature.x > BORDER_RIGHT) {
        creature.x = BORDER_RIGHT;
        changeDirection(creature);
    }

    if (creature.y < radius) {
        creature.y = radius;
        changeDirection(creature);
    }

    if (creature.y > BORDER_TOP) {
        creature.y = BORDER_TOP;
        changeDirection(creature);
    }
}

function changeDirection(creature) {
    // Multiple speedX and speedY with a random numer (1 or -1)
    creature.speedX *= randomDirection(2);
    creature.speedY *= randomDirection(2);
}

// Write many small functions to express your intentions.
function randomDirection() {
    if (randomNumber(2))
        return 1;

    return -1;
}

function randomSpeed() {
    return randomNumber(MAXIMUM_SPEED);
}

// Create a random positive integer smaller than the supplied value of MAX.
function randomNumber(max) {
    return Math.floor(Math.random() * max);
}