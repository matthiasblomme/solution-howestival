/*
ADD WALL COLLISIONS

- Add a speedX & speedY property to the creature json object.

- Add Collision check for the x-axis.
    - Write an OK to console for each succesful boundary case.

- Add Collision check for the y-axis.
    - Use constants to give a name to difficult expressions.

- Notice all the hard codes numbers!
    - Refactor to constants.

- Remove the console statements and replace them with a change direction function!
    - Let the creature choose a random direction upon collisions!
        - Change direction means making making the speed negative or positive.
        - With each collision both speedX and speedY get a random 1 or -1 value.
        - Create a function which creates a random positive integer!

- When the direction is changed, move the creature.
    - Use the new speed values to change the X and Y positions.
    

PLEASE COMPLETE THE INSTRUCTIONS MARKED BY ...some instruction...


---------------------------------------------------------------------------------------------
*/

const CANVAS_WIDTH = 400;
//...Try to use many constants.

let creature;

function setup() {
    createCanvas(CANVAS_WIDTH, CANVAS_HEIGHT);
    createCreature();
}

function draw() {
    background(BACKGROUND_WHITE);
    //... Before the creature is redrawn make sure there are no collisions...
    drawCreature(creature);
    moveCreature(creature);
}

function createCreature() {
    creature = {
        x: 100,
        y: 100,
        //...Add a speedX and a speedY value with some value of choice...
        diameter: 40
    };
}

function drawCreature(creature) {
    let c = color("gold")
    ellipse(creature.x, creature.y, creature.diameter, creature.diameter);
    fill(c);
    noStroke();
}

function moveCreature(creature) {
    creature.x += creature.speedX;
    //...Let the creature also use the x-axis...
}

function checkBorderCollision(creature) {
    //...create a radius variable...
    //...radius is diamater / 2...

    const BORDER_RIGHT = CANVAS_WIDTH - radius;

    // If the center of the creature is smaller dan iets radius, then it touches the left wall.
    if (creature.x < radius) {
        creature.x = radius;
        changeDirection(creature);
    }

    if (creature.x > BORDER_RIGHT) {
        creature.x = BORDER_RIGHT;
        changeDirection(creature);
    }

    //...Check for top collisions...

    //...Check for bottom collision...
}

function changeDirection(creature) {
    //...multiple speedX and speedY with a random numer (1 or -1)
    //...use the randomDirection function()...
    //...check the moveCreature function for inspiration...
}

function randomDirection() {
    //...if the randomNumber is positive return 1...
    //...else return -1...
}

function randomSpeed() {
    //...use the randomNumber function to create a random speed...
}

// Create a random positive integer smaller than the supplied value of MAX.
function randomNumber(max) {
    return Math.floor(Math.random() * max);
}