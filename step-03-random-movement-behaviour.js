/*
LET THE CREATURE MOVE AT RANDOM

- Add a changeToMove property to the creature object.

- Change the moveCreature function to allow random movements:
    - Create a random number.
    - When it is smaller than it creatures chance to move -> changeDirection()

*/

const CANVAS_WIDTH = 400;
const CANVAS_HEIGHT = 400;
const BACKGROUND_WHITE = 240;
const MAXIMUM_SPEED = 20;
const MAXIMUM_RANDOMNESS = 100;

let creature;

function setup() {
    createCanvas(CANVAS_WIDTH, CANVAS_HEIGHT);
    createCreature();
}

function draw() {
    background(BACKGROUND_WHITE);
    checkBorderCollision(creature);
    drawCreature(creature);
    moveCreature(creature);
}

function createCreature() {
    creature = {
        x: 100,
        y: 100,
        speedX: randomSpeed(),
        speedY: randomSpeed(),
        // A property to determine the chance the change it's direction at random. 
        chanceToMove: 20,
        diameter: 40
    };
}

function drawCreature(creature) {
    let c = color("gold")
    ellipse(creature.x, creature.y, creature.diameter, creature.diameter);
    fill(c);
    noStroke();
}

function moveCreature(creature) {
    // Generate a random number, if it's smaller than the chance to move, change direction!
    if (randomNumber(MAXIMUM_RANDOMNESS) < creature.chanceToMove) {
        changeDirection(creature)
    }

    creature.x += creature.speedX;
    creature.y += creature.speedY;
}

function checkBorderCollision(creature) {
    let radius = creature.diameter / 2;
    const BORDER_RIGHT = CANVAS_WIDTH - radius;
    const BORDER_TOP = CANVAS_HEIGHT - radius;

    if (creature.x < radius) {
        creature.x = radius;
        changeDirection(creature);
    }

    if (creature.x > BORDER_RIGHT) {
        creature.x = BORDER_RIGHT;
        changeDirection(creature);
    }

    if (creature.y < radius) {
        creature.y = radius;
        changeDirection(creature);
    }

    if (creature.y > BORDER_TOP) {
        creature.y = BORDER_TOP;
        changeDirection(creature);
    }
}

function changeDirection(creature) {
    // Multiple speedX and speedY with a random numer (1 or -1)
    creature.speedX *= randomDirection(2);
    creature.speedY *= randomDirection(2);
}

function randomDirection() {
    if (randomNumber(2))
        return 1;

    return -1;
}

function randomSpeed() {
    return randomNumber(MAXIMUM_SPEED);
}

function randomNumber(max) {
    return Math.floor(Math.random() * max);
}