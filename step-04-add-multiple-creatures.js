/*
ADD MANY CREATURES WITH RANDOM BEHAVIOUR AND COLOR 

- Create a creatures array. 
    - We don't need the creature variable anymore. 

- Create a constant NUMBER_OF_CREATURES to limit the number of creatures. 
    - Assign a value to it. 

- The setup function is the place to initialize all creatures 
    - Make a function createCreatures 

    - With a for statement loop NUMBER_OF_CREATURES 
        - For each iteration call the function createCreature() 
        - Let the createCreature function return a the json object instead of assigning. 
        - The new creature is added to the creatures array. 

- Create a new function spawnCreatures() and call it in the draw() function. 
    - This new function will spawn all creatures by calling spawnCreature NUMBER_OF_CREATURES times. 
    - Each time a creature is taken from the array and passed to the spawnCreature function 

- Create the spawnCreature(creature) function 
    - Invoke the checkBorderCollision function. 
    - Invoke the moveCreature function. 
    - Invoke the drawCreature function. 

- Notice that now all creatures have the same size, color and starting position. 
    - Let's improve this. 

    - Make the starting position random! 
        - Assign a random value to creature.x & y. (What is the maximum value?) 

    - Make the size random for each creature! 
        - Create a function randomDiameter 
            - Make sure to have a minimum and maximum value. 
            - Use constants! 

    - Make the color random! 
        - Create a randomColor function. 
            - Use the function color(redValue, greenValue, blueValue) 
            - Make sure all arguments are random values (max 230) 
            - E.g., color(randomNumber(230), ...  

    - Make the chance to move random 
        - Create a randomChanceToMove function. 
            - Again, think about minimum and maximum values ;) 

- Behold the spectacle! 
*/

const CANVAS_WIDTH = 400;
const CANVAS_HEIGHT = 400;
const BACKGROUND_WHITE = 240;
const MAXIMUM_SPEED = 5;
const MAXIMUM_RANDOMNESS = 100;
const NUMBER_OF_CREATURES = 50;
const CREATURE_MIN_WIDTH = 10;

// We don't need the creature variable anymore.
let creatures = [];

function setup() {
    createCanvas(CANVAS_WIDTH, CANVAS_HEIGHT);
    createCreatures();
}

function draw() {
    background(BACKGROUND_WHITE);
    spawnCreatures();
}

function createCreatures() {
    // Repeat NUMBER_OF_CREATURE times. In this case 50 times.
    for (let i = 0; i < NUMBER_OF_CREATURES; i++) {
        // Create the creature.
        let creature = createCreature()
        // Assign it to the creatures array.
        creatures.push(creature)
    }
}

function createCreature() {
    // Return the object in stead of assigning it to the creature variable.
    return {
        // ALL properties should be random to improve our game.
        x: randomNumber(CANVAS_WIDTH),
        y: randomNumber(CANVAS_HEIGHT),
        speedX: randomSpeed(),
        speedY: randomSpeed(),
        chanceToMove: randomChanceToMove(),
        diameter: randomDiameter(),
        color: randomColor()
    };
}

function spawnCreatures() {
    // Loop over all the creature, use a forEach statement.
    creatures.forEach(creature => spawnCreature(creature));
}

function spawnCreature(creature) {
    checkBorderCollision(creature);
    moveCreature(creature);
    drawCreature(creature);
}

function drawCreature(creature) {
    ellipse(creature.x, creature.y, creature.diameter, creature.diameter);
    fill(creature.color);
    noStroke();
}

function moveCreature(creature) {
    if (randomNumber(MAXIMUM_RANDOMNESS) < creature.chanceToMove) {
        changeDirection(creature)
    }

    creature.x += creature.speedX;
    creature.y += creature.speedY;
}

function checkBorderCollision(creature) {
    let radius = creature.diameter / 2;
    const BORDER_RIGHT = CANVAS_WIDTH - radius;
    const BORDER_TOP = CANVAS_HEIGHT - radius;

    if (creature.x < radius) {
        creature.x = radius;
        changeDirection(creature);
    }

    if (creature.x > BORDER_RIGHT) {
        creature.x = BORDER_RIGHT;
        changeDirection(creature);
    }

    if (creature.y < radius) {
        creature.y = radius;
        changeDirection(creature);
    }

    if (creature.y > BORDER_TOP) {
        creature.y = BORDER_TOP;
        changeDirection(creature);
    }
}

function changeDirection(creature) {
    creature.speedX *= randomDirection(2);
    creature.speedY *= randomDirection(2);
}

function randomChanceToMove() {
    return randomNumber(10) + 5;
}

function randomDiameter() {
    return randomNumber(30) + CREATURE_MIN_WIDTH;
}

function randomColor() {
    return color(randomNumber(232), randomNumber(230), randomNumber(230));
}

function randomDirection() {
    if (randomNumber(2))
        return 1;

    return -1;
}

function randomSpeed() {
    return randomNumber(MAXIMUM_SPEED) + 1; 
}

function randomNumber(max) {
    return Math.floor(Math.random() * max);
}