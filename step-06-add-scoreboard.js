/*
# ADD A SCOREBOARD

- add renderScores function and add below renderCreatures in draw
- add renderScore line in renderScore function
- refactor to:
    - sortCreatures
    - renderScoreLines
*/

const CANVAS_WIDTH = 800;
const CANVAS_HEIGHT = 800;
const BACKGROUND_WHITE = 240;
const MAXIMUM_SPEED = 5;
const MAXIMUM_RANDOMNESS = 200;
const NUMBER_OF_CREATURES = 50;
const CREATURE_MIN_WIDTH = 10;
const MAX_DIAMETER = 50;

let creatures = [];

function setup() {
    createCanvas(CANVAS_WIDTH, CANVAS_HEIGHT);
    createCreatures();
}

function draw() {
    background(BACKGROUND_WHITE);
    spawnCreatures();
    renderScores();
}

function createCreatures() {
    for (let i = 0; i < NUMBER_OF_CREATURES; i++) {
        let creature = createCreature(i);
        creatures.push(creature);
    }
}

function createCreature(id) {
    let diameter = randomDiameter();
    return {
        id: id,
        x: randomNumber(CANVAS_WIDTH),
        y: randomNumber(CANVAS_HEIGHT),
        speedX: randomSpeed(),
        speedY: randomSpeed(),
        chanceToMove: randomChanceToMove(),
        diameter: diameter,
        // New property strength
        strength: diameter,
        color: randomColor()
    };
}

function spawnCreatures() {
    creatures.forEach(creature => spawnCreature(creature));
}

function spawnCreature(creature) {
    checkBorderCollision(creature);
    // Check for creature collisions
    checkCreatureCollision(creature);
    moveCreature(creature);
    drawCreature(creature);
}

function drawCreature(creature) {
    fill(creature.color);
    noStroke();
    ellipse(creature.x, creature.y, creature.diameter, creature.diameter);
}

function moveCreature(creature) {
    if (randomNumber(MAXIMUM_RANDOMNESS) < creature.chanceToMove) {
        changeDirection(creature)
    }

    creature.x += creature.speedX;
    creature.y += creature.speedY;
}

function checkBorderCollision(creature) {
    let radius = creature.diameter / 2;
    const BORDER_RIGHT = CANVAS_WIDTH - radius;
    const BORDER_TOP = CANVAS_HEIGHT - radius;

    if (creature.x < radius) {
        creature.x = radius;
        changeDirection(creature);
    }

    if (creature.x > BORDER_RIGHT) {
        creature.x = BORDER_RIGHT;
        changeDirection(creature);
    }

    if (creature.y < radius) {
        creature.y = radius;
        changeDirection(creature);
    }

    if (creature.y > BORDER_TOP) {
        creature.y = BORDER_TOP;
        changeDirection(creature);
    }
}

function checkCreatureCollision(source) {
    // Loop over all the creatures and find the ones with a collision.
    creatures.forEach(target => {
        // Make sure to check that the target is not  yourself AND there is a collision.
        if (isDifferent(source, target) && isCreatureCollision(source, target))
            fight(source, target);
    });
}

function fight(source, target) {
    // Creatures with a high strength have a bigger chance to win, but this is not necessarly true.
    let sourceStrength = randomNumber(source.strength);
    let targetStrength = randomNumber(target.strength);

    // If the source creature is stronger than the target creature -> eat the target.
    if (sourceStrength > targetStrength) {
        eatCreature(source, target);

        // If the target creature is stronger than the source creature -> eat the source.
    } else if (sourceStrength < targetStrength) {
        eatCreature(target, source);
    } else {
        // If the strength is equal, both die during battle.
        deleteCreature(target);
        deleteCreature(source);
    }
}

// Check is the source creature is not the target creature.
function isDifferent(source, target) {
    return source.id != target.id;
}

// Calculates if the source and target are colliding.
function isCreatureCollision(source, target) {
    let d = dist(source.x, source.y, target.x, target.y);
    return d < (source.diameter / 2 + target.diameter / 2);
}

function eatCreature(hunter, prey) {
    changeStats(hunter, prey);
    checkDiameter(hunter);
    deleteCreature(prey);
}

function changeStats(hunter, prey) {
    // The hunter increases it's strength with the strength of the prey.
    hunter.strength += prey.strength;
    // The hunter increases it's diameter with the diameter of the prey.
    hunter.diameter += prey.diameter;
}

// This function limits the growth of the creature.
// Otherwise it would be bigger then the screen.
function checkDiameter(creature) {
    if (creature.diameter > MAX_DIAMETER)
        creature.diameter = MAX_DIAMETER
}

// Filter out the creature from the creatures array.
// This will create a new array.
// Assign this array back to the creatures array.
function deleteCreature(target) {
    creatures = creatures.filter(creature => creature.id != target.id);
}

function changeDirection(creature) {
    creature.speedX *= randomDirection(2);
    creature.speedY *= randomDirection(2);
}

function randomChanceToMove() {
    return randomNumber(10) + 5;
}

function randomDiameter() {
    return randomNumber(10) + CREATURE_MIN_WIDTH;
}

function randomColor() {
    return color(randomNumber(232), randomNumber(230), randomNumber(230));
}

function randomDirection() {
    if (randomNumber(2))
        return 1;

    return -1;
}

function randomSpeed() {
    return randomNumber(MAXIMUM_SPEED) + 1;
}

function randomNumber(max) {
    return Math.floor(Math.random() * max);
}

function renderScores() {
    // Define space between each score lines.
    let space = 20;
    // Draw the score line 30 pixels from the top.
    let top = 30
    fill(color("black"));
    // Add some size to the text
    textSize(space);

    // Sort the array from biggest strength to lowest strength
    sortCreatures();

    renderScoreLines(top, space);
}

function sortCreatures() {
    creatures.sort(function (a, b) {
        return b.strength - a.strength;
    });
}

function renderScoreLines(top, space) {
    let position = 1;
    // For each creature draw a score line.
    creatures.forEach(creature => {
        renderScoreLine(creature, top, position)
        // After each score line the top must be increased for the next score line.
        top += space;
        // Increase the rank for the next creature.
        position++;

    })
}
function renderScoreLine(creature, top, position) {
    fill(color(creature.color));
    text("[" + position + "] " + creature.strength, 10, top);
}